let string1 = (value) => {


    if (typeof value == 'string') {

        let output = value.replace("$", "")
            .replace(",", "");

        if (isNaN(output)) {

            return 0;
        } else {

            return parseFloat(output);
        }
    } else {

        return 0;
    }

}


module.exports = string1;