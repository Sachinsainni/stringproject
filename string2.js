const string2 = ((value) => {

    if (typeof value == 'string') {

        return value.split('.').map((x) => {
            if (!isNaN(x)) {
                return parseInt(x);
            } else{
                return []
            }
        })

    } else {
        return [];
    }
})
module.exports = string2;