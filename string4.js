const string4 =(obj=>{
        let firstName = (obj.first_name).toUpperCase() ;
        let lastName = (obj.last_name).toUpperCase();
        let fullName ;
        if (obj.middle_name == undefined){
          fullName   = `${firstName.charAt(0)+ firstName.substr(1).toLowerCase()} ${lastName.charAt(0) + lastName.substr(1).toLowerCase()}`
        }
        else{
            let middleName = (obj.middle_name).toUpperCase();
            fullName = `${firstName.charAt(0) + firstName.substr(1).toLowerCase()} ${middleName.charAt(0) + middleName.substr(1).toLowerCase()} ${lastName.charAt(0) + lastName.substr(1).toLowerCase()}`
        }
        return fullName;
      
})
module.exports = string4 ; 